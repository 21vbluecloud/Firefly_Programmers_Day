# 萤火虫程序节

## 2.寻找消失的数字
> 问题描述：寻找消失的数字,给你一个数组{0,1,2,3,n},其中有一个数字是缺失的,在最快时间内,请把缺失的数字找出来(程序语言不限)。
```diff
- 首先纠个错，题目写的有问题，写的是数组``[]``，实际给的是集合``{}``
```

### 设计思路
数组的长度是固定的，可以计算整个数组的期望和，然后减去实际数组的总和，差值就是缺失的数字。

- Python代码如下：

```python
def find_missing_number(nums):
    n = len(nums)
    expected_sum = (n * (n + 1)) // 2  # 期望和
    actual_sum = sum(nums)  # 实际和
    missing_number = expected_sum - actual_sum
    return missing_number
```
- 运行测试：
```python
nums = [0, 1, 2, 3, 4, 5, 6, 8]  # 7 缺失
missing_number = find_missing_number(nums)
print("缺失的数字是:", missing_number) # 打印 7
```



### 作品特点：
简单。





