def find_missing_number(nums):
    n = len(nums)
    expected_sum = (n * (n + 1)) // 2  # 期望和
    actual_sum = sum(nums)  # 实际和
    missing_number = expected_sum - actual_sum
    return missing_number


nums = [0, 1, 2, 3, 4, 5, 6, 8]  # 7 缺失
missing_number = find_missing_number(nums)
print("缺失的数字是:", missing_number) # 打印 7

